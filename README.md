# JULO Mini Wallet Service

---

A mock backend service created with MongoDB, Express, and NodeJS.

Response format based on: [JSend](https://github.com/omniti-labs/jsend).

OS Tested: Windows 10 Home, Ubuntu Focal.

## Installation Guide

### Requirements
- Docker.
- Git (if you don't want to manually download).
- MongoDB (if you don't have Docker).
- NodeJS.
- Yarn.

### Setting Up (Windows)
1. Download and install NodeJS from the [official site](https://nodejs.org/en/).
   1. Optionally. You can use [NVM](https://github.com/coreybutler/nvm-windows) instead.
2. Run the command `npm install -g yarn` to install yarn.
4. Git clone the project or download and extract zip file in preferred directory.
   1. Optional. Only if cloning project. Download and install [git](https://git-scm.com/downloads).
5. Change directory into project folder then run the following command.
```
yarn install
```
6. Create temporary MongoDB container with this command `docker run --name mongodb --rm -p 27017:27017 mongo`.
   1. **[Not Recommended]**. Optional. Install MongoDB Community. Follow the instruction in the [official site](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/).
7. Create environment file (.env) at root directory. You should use .env.example content as guide.
8. To start the application, run the command `yarn start`.

### Setting Up (Ubuntu)
1. Run the following command to install NVM and use Node 16.13.2.
```
sudo apt install curl -y
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

# You need to exit the terminal before running the command below.
nvm install 16.13.2
# Ensure you are running 16.13.2
nvm current
```
2. Run the command `npm install -g yarn` to install yarn.
3. Git clone the project or download and extract zip file in preferred directory.
   1. Optional. Only if cloning project. Install git with this command `apt install git -y`
4. Change directory into project folder then run the following command.
```
yarn install
```
5. Create temporary MongoDB container with this command `docker run --name mongodb --rm -p 27017:27017 mongo`.
   1. **[Not Recommended]**. Optional. Install MongoDB Community. Follow the instruction in the [official site](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/).
6. Create environment file (.env) at root directory. You should use .env.example content as guide.
7. To start the application, run the command `yarn start`.

## Automated API Test Guide

**Important Note:**
- No isolated test environment for API testing. Any data created or deleted due to running tests will not be restored in the database.
- The tests will react strangely when run without a clean database. This is because the test depend heavily on assumption that any data within the database are readily consumed for test.
- TLDR: Only run tests when the database is clean.

To run the automated API tests, simply run the command below:
```
yarn test:api
```