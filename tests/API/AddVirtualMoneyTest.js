const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');

const Account = require('../../src/models/Account');
const Transaction = require('../../src/models/Transaction');
const Wallet = require('../../src/models/Wallet');

chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

describe('Add Virtual Money', () => {

    before(async () => {
        let createdAccounts = await Account.create([
            {customer_xid: 'has-wallet-customer-xid', token: 'has-wallet-account-token'},
        ]);

        await Wallet.create({
            owned_by: createdAccounts[0]._id,
            status:  true,
            updated_at: new Date(),
            balance: 0
        });

        return true;
    });

    after(async () => {

        await Account.deleteMany({});
        await Transaction.deleteMany({});
        await Wallet.deleteMany({});

        return true;
    });

    describe('POST /deposits with valid authorization and body', () => {
        it('should get created deposit data',  (done) => {
            chai.request(app)
                .post('/api/v1/wallet/deposits')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 100000, reference_id: 'random-reference-id'})
                .end((error, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['deposit']);
                    done();
                });
        });
    });

    describe('POST /deposits with invalid authorization', () => {
        it('should return a 401 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/deposits')
                .set({'Authorization': 'Token blob'})
                .send({amount: 50000, reference_id: 'random-reference-id-2'})
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('POST /deposits with missing reference_id', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/deposits')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 20000})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['reference_id']);
                    done();
                });
        });
    });

    describe('POST /deposits with missing amount', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/deposits')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({reference_id: 'random-reference-id-3'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['amount']);
                    done();
                });
        });
    });

    describe('POST /deposits with duplicate reference_id', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/deposits')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 20000, reference_id: 'random-reference-id'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['reference_id']);
                    done();
                });
        });
    });

    describe('POST /deposits with amount of negative', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/deposits')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: -50000, reference_id: 'random-reference-id'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['amount']);
                    done();
                });
        });
    });
});