const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');

const Account = require('../../src/models/Account');
const Wallet = require('../../src/models/Wallet');

chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

describe('Disable My Wallet', () => {

    before(async () => {
        let createdAccounts = await Account.create([
            {customer_xid: 'has-disabled-wallet-customer-xid', token: 'has-disabled-wallet-account-token'},
            {customer_xid: 'has-enabled-wallet-test-customer2', token: 'has-enabled-wallet-account-token2'},
            {customer_xid: 'has-enabled-wallet-test-customer', token: 'has-enabled-wallet-account-token'}
        ]);

        await Wallet.create({
                owned_by: createdAccounts[0]._id,
                status: false,
                balance: 0,
                updated_at: new Date()
            },
            {
                owned_by: createdAccounts[1]._id,
                status: true,
                balance: 0,
                updated_at: new Date()
            },
            {
                owned_by: createdAccounts[2]._id,
                status: true,
                balance: 0,
                updated_at: new Date()
            });

        return true;
    });

    after(async () => {
        await Account.deleteMany({});
        await Wallet.deleteMany({});

        return true;
    });

    describe('PATCH /wallet with authorization and correct body', () => {
        it('should successfully disable wallet', (done) => {
            chai.request(app)
                .patch('/api/v1/wallet')
                .set({'Authorization': 'Token has-enabled-wallet-account-token'})
                .send({is_disabled: true})
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('PATCH /wallet with authorization and incorrect body', () => {
        it('should get a 422 status code error', (done) => {
            chai.request(app)
                .patch('/api/v1/wallet')
                .set({'Authorization': 'Token has-enabled-wallet-account-token2'})
                .send({is_disabled: false})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('PATCH /wallet with incorrect authorization', () => {
        it('should get a 401 status code error', (done) => {
            chai.request(app)
                .patch('/api/v1/wallet')
                .set({'Authorization': 'Token blob'})
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('PATCH /wallet with no authorization', () => {
        it('should get a 401 status code error', (done) => {
            chai.request(app)
                .patch('/api/v1/wallet')
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('PATCH /wallet with authorized account that have disabled wallet', () => {
        it('should get a 422 status code error', (done) => {
            chai.request(app)
                .patch('/api/v1/wallet')
                .set({'Authorization': 'Token has-disabled-wallet-account-token'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });
});