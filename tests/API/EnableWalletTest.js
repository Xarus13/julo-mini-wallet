const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');

const Account = require('../../src/models/Account');
const Wallet = require('../../src/models/Wallet');

chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

describe('Enable My Wallet', () => {

    before(async () => {
        let createdAccounts = await Account.create([
            {customer_xid: 'no-wallet-customer-xid', token: 'no-wallet-account-token'},
            {customer_xid: 'has-wallet-test-customer', token: 'has-wallet-account-token'},
            {customer_xid: 'disabled-wallet-test-customer', token: 'disabled-wallet-account-token'}
        ]);

        await Wallet.create({
                owned_by: createdAccounts[1]._id,
                status: true,
                updated_at: new Date(),
                balance: 0
            },
            {
                owned_by: createdAccounts[2]._id,
                status: false,
                updated_at: new Date(),
                balance: 0
            });

        return true;
    });

    after(async () => {
        await Account.deleteMany({});
        await Wallet.deleteMany({});

        return true;
    });

    describe('POST /wallet with authorization', () => {
        it('should get a wallet', (done) => {
            chai.request(app)
                .post('/api/v1/wallet')
                .set({'Authorization': 'Token no-wallet-account-token'})
                .end((error, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('POST /wallet with incorrect authorization', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet')
                .set({'Authorization': 'Token blob'})
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('POST /wallet with no authorization', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet')
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('POST /wallet with authorized account that have wallet', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .end((error, response) => {
                    response.should.have.status(409);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('POST /wallet with authorized account that have disabled wallet', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet')
                .set({'Authorization': 'Token disabled-wallet-account-token'})
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });
});