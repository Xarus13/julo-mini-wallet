const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');
const Account = require("../../src/models/Account");

chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

describe('Initialize My Account For Wallet', () => {

    before(async () => {
        await Account.create({customer_xid: '7a3ea523-da67-4d15-ab89-1a0016170e8c', token: 'account-test-token-with-uuid'});

        return true;
    });

    after(async () => {
        await Account.deleteMany({});

        return true;
    });

    describe('POST /init with given customer_xid', () => {
        it('should create an account with token', (done) => {
            chai.request(app)
                .post('/api/v1/init')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({customer_xid: 'ea0212d3-abd6-406f-8c67-868e814a2436'})
                .end((error, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data').that.includes.all.keys(['token']);
                    done();
                });
        });
    });

    describe('POST /init with customer_xid not UUID format', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/init')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({customer_xid: 'customer-test-data'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.have.property('status');
                    response.body.should.have.property('data').that.includes.all.keys(['customer_xid']);
                    done();
                });
        });
    });

    describe('POST /init with existing customer_xid', () => {
        it('should return a fail response', (done) => {
            chai.request(app)
                .post('/api/v1/init')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({customer_xid: '7a3ea523-da67-4d15-ab89-1a0016170e8c'})
                .end((error, response) => {
                    response.should.have.status(409);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data').that.includes.all.keys(['customer_xid']);
                    done();
                });
        });
    });

    describe('POST /init without given customer_xid', () => {
        it('should get a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/init')
                .set('content-type', 'application/x-www-form-urlencoded')
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.have.property('status');
                    response.body.should.have.property('data').that.includes.all.keys(['customer_xid']);
                    done();
                });
        });
    });
});