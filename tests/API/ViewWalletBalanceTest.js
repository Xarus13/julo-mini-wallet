const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');
const Account = require("../../src/models/Account");
const Wallet = require('../../src/models/Wallet');

chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

describe('View Wallet Balance', () => {

    before(async () => {
        let createdAccounts = await Account.create([
            {customer_xid: 'test-customer-xid', token: 'test-account-token'},
            {customer_xid: 'no-wallet-test-customer', token: 'no-wallet-account-token'},
            {customer_xid: 'disabled-wallet-test-customer', token: 'disabled-wallet-account-token'}
        ]);

        await Wallet.create({
            owned_by: createdAccounts[0]._id,
            status: true,
            updated_at: new Date(),
            balance: 0
        });

        await Wallet.create({
            owned_by: createdAccounts[2]._id,
            status: false,
            updated_at: new Date(),
            balance: 0
        });

        return true;
    });

    after(async () => {
        await Account.deleteMany({});
        await Wallet.deleteMany({});

        return true;
    });

    describe('GET /wallet when succeed authorization', () => {
        it('should get a wallet', (done) => {
            chai.request(app)
                .get('/api/v1/wallet')
                .set({'Authorization': 'Token test-account-token'})
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('GET /wallet when fail authorization', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .get('/api/v1/wallet')
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('GET /wallet when account wallet is disabled', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .get('/api/v1/wallet')
                .set({'Authorization': 'Token disabled-wallet-account-token'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });

    describe('GET /wallet when account does not have wallet', () => {
        it('should get an error', (done) => {
            chai.request(app)
                .get('/api/v1/wallet')
                .set({'Authorization': 'Token no-wallet-account-token'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('status');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });
});