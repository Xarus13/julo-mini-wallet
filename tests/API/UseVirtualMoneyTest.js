const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');

const Account = require('../../src/models/Account');
const Transaction = require('../../src/models/Transaction');
const Wallet = require('../../src/models/Wallet');

chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

describe('Use Virtual Money', () => {

    before(async () => {
        let createdAccounts = await Account.create([
            {customer_xid: 'has-wallet-customer-xid', token: 'has-wallet-account-token'},
        ]);

        await Wallet.create({
            owned_by: createdAccounts[0]._id,
            status: true,
            updated_at: new Date(),
            balance: 1000000
        });

        await Transaction.create({
            type: 'Deposit',
            trigger_by: createdAccounts[0]._id,
            status: 'success',
            execute_at: new Date,
            amount: 1000000,
            reference_id: 'random-deposit-transaction-id'
        });

        return true;
    });

    after(async () => {

        await Account.deleteMany({});
        await Transaction.deleteMany({});
        await Wallet.deleteMany({});

        return true;
    });

    describe('POST /withdrawals with valid authorization and body', () => {
        it('should get created transaction (withdrawal) data',  (done) => {
            chai.request(app)
                .post('/api/v1/wallet/withdrawals')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 100000, reference_id: 'use-virtual-money-id'})
                .end((error, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['withdrawal']);
                    done();
                });
        });
    });

    describe('POST /withdrawals with invalid authorization', () => {
        it('should return a 401 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/withdrawals')
                .set({'Authorization': 'Token blob'})
                .send({amount: 50000, reference_id: 'use-virtual-money-id2'})
                .end((error, response) => {
                    response.should.have.status(401);
                    response.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('POST /withdrawals with missing body', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/withdrawals')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 20000})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['reference_id']);
                    done();
                });
        });
    });

    describe('POST /withdrawals with missing amount', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/withdrawals')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({reference_id: 'use-virtual-money-id3'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['amount']);
                    done();
                });
        });
    });

    describe('POST /withdrawals with duplicate reference_id', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/withdrawals')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 100000, reference_id: 'use-virtual-money-id'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data').that.includes.all.keys(['reference_id']);
                    done();
                });
        });
    });

    describe('POST /withdrawals with amount > wallet.balance', () => {
        it('should return a 422 status code error', (done) => {
            chai.request(app)
                .post('/api/v1/wallet/withdrawals')
                .set({'Authorization': 'Token has-wallet-account-token'})
                .send({amount: 2000000, reference_id: 'use-virtual-money-id2'})
                .end((error, response) => {
                    response.should.have.status(422);
                    response.body.should.be.a('object');
                    response.body.should.have.property('data');
                    done();
                });
        });
    });
});