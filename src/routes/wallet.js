const express = require('express');
const router = express.Router();

const {authHandler} = require('../helpers/AuthHandler');

const {getWalletBalance} = require('../controllers/ViewWalletBalance');
const {enableWallet} = require('../controllers/EnableWallet');
const {disableWallet} = require('../controllers/DisableWallet');
const {addVirtualMoney} = require('../controllers/AddVirtualMoney');
const {useVirtualMoney} = require('../controllers/UseVirtualMoney');

const {validatePostWalletDeposit} = require('../validator/PostWalletDepositValidator');
const {validatePostWalletWithdrawal} = require('../validator/PostWalletWithdrawalValidator');
const {validatePatchWalletDisabled} = require('../validator/PatchDisableWalletValidator');

router.use((request, response, next) => {
    authHandler(request, response, next);
});

router.get('/', async (request, response, next) => {
    await getWalletBalance(request, response, next);
});

router.post('/', async (request, response, next) => {
    await enableWallet(request, response, next);
});

router.patch('/', validatePatchWalletDisabled, async (request, response, next) => {
    await disableWallet(request, response, next);
});

router.post('/deposits', validatePostWalletDeposit, async (request, response, next) => {
    await addVirtualMoney(request, response, next);
});

router.post('/withdrawals', validatePostWalletWithdrawal, async (request, response, next) => {
    await useVirtualMoney(request, response, next);
});

module.exports = router;