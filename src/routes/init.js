const express = require('express');
const router = express.Router();

const initializeAccount = require('../controllers/InitializeAccount');

const {validatePostInitializeAccount} = require('../validator/PostInitializeAccountValidator');

router.post('/', validatePostInitializeAccount, async (request, response, next) => {
    await initializeAccount.createAccount(request, response, next);
});

module.exports = router;