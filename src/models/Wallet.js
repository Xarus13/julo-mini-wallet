const mongoose = require('mongoose');

const schema = mongoose.Schema({
    owned_by: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        required: true
    },
    balance: {
        type: Number,
        default: 0
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Wallet', schema);