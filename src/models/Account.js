const mongoose = require('mongoose');

const schema = mongoose.Schema({
    customer_xid: {
        type: String,
        required: true,
        unique: true
    },
    token: {
        type: String
    },
});

module.exports = mongoose.model('Account', schema);