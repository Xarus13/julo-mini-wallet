const mongoose = require('mongoose');

const schema = mongoose.Schema({
    type: {
        type: String,
        enum: ['Deposit', 'Withdrawal'],
        required: true
    },
    trigger_by: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    execute_at: {
        type: Date
    },
    amount: {
        type: Number,
        required: true
    },
    reference_id: {
        type: String,
        required: true,
        unique: true
    }
});

module.exports = mongoose.model('Transaction', schema);