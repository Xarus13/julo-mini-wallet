const {check, validationResult} = require('express-validator');
const {ErrorHandler} = require("../helpers/ErrorHandler");

exports.validatePatchWalletDisabled = [
    check('is_disabled')
        .isBoolean()
        .withMessage('is_disabled is required')
        .custom(value => {
            if (value) return Promise.resolve();
            else return Promise.reject('is_disabled only accept (Boolean) true')
        }),
    (request, response, next) => {
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            let data = {};
            for (let {msg, param} of errors.array()) {
                data[param] = msg;
            }

            throw new ErrorHandler(422, data);
        }
        next();
    }
];