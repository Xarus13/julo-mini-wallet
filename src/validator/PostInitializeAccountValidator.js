const {check, validationResult} = require('express-validator');
const {ErrorHandler} = require("../helpers/ErrorHandler");

exports.validatePostInitializeAccount = [
    check('customer_xid')
        .isString()
        .withMessage('customer_xid must be a string')
        .notEmpty()
        .withMessage('customer_xid is required')
        .custom(value => {
            const pattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
            const isUUID = pattern.test(value);

            if (isUUID) return Promise.resolve();
            else return Promise.reject('customer_xid must be UUID.');
        }),
    (request, response, next) => {
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            let data = {};
            for (let {msg, param} of errors.array()) {
                data[param] = msg;
            }

            throw new ErrorHandler(422, data);
        }
        next();
    }
];