const {check, validationResult} = require('express-validator');
const {ErrorHandler} = require("../helpers/ErrorHandler");

const Transaction = require('../models/Transaction');

exports.validatePostWalletDeposit = [
    check('amount')
        .notEmpty()
        .withMessage('amount is required')
        .isInt({min: 1})
        .withMessage('amount must be a nominal larger than 1'),
    check('reference_id')
        .notEmpty()
        .withMessage('reference_id is required')
        .custom(value => {
            return Transaction.findOne({reference_id: value})
                .then((object) => {
                    if (object !== null) return Promise.reject('reference_id must be unique')
                });
        }),
    (request, response, next) => {
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            let data = {};
            for (let {msg, param} of errors.array()) {
                data[param] = msg;
            }

            throw new ErrorHandler(422, data);
        }
        next();
    }
];