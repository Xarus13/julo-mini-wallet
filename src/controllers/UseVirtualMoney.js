require('dotenv').config();
const Wallet = require('../models/Wallet');
const Transaction = require('../models/Transaction');
const {ErrorHandler} = require("../helpers/ErrorHandler");


exports.useVirtualMoney = async (request, response, next) => {
    const {amount, reference_id} = request.body;
    const user = response.locals.user;

    try {
        let wallet = await Wallet.findOne({owned_by: user._id});

        if (wallet === undefined || wallet === null) throw new ErrorHandler(404, 'This user does not have a wallet');
        if (wallet.status === false) throw new ErrorHandler(400, 'Unable to execute transaction. Wallet is currently disabled');
        if (wallet.balance < amount) throw new ErrorHandler(422, 'There is not enough balance to execute this transaction');

        let transaction = await Transaction.create({
            type: 'Withdrawal',
            trigger_by: user._id,
            status: 'success',
            execute_at: new Date,
            amount,
            reference_id
        });

        wallet.balance -= amount;
        await wallet.save(() => {
            response.status(201).send({
                status: 'success',
                data: {
                    withdrawal: {
                        id: transaction._id,
                        withdrawn_by: user._id,
                        status: 'success',
                        withdrawn_at: transaction.execute_at,
                        amount,
                        reference_id
                    }
                }
            });
        });

    } catch (error) {
        next(error);
    }
}