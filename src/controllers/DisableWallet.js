require('dotenv').config();
const wallet = require('../models/Wallet');
const Wallet = require("../models/Wallet");
const {ErrorHandler} = require("../helpers/ErrorHandler");

exports.disableWallet = async (request, response, next) => {
    const {is_disabled} = request.body;
    let user = response.locals.user;

    try {
        let currentWallet = await Wallet.findOne({owned_by: user._id});

        if (currentWallet === null || currentWallet === undefined) throw new ErrorHandler(404, 'Wallet does not exist');
        if (currentWallet.status === false) throw new ErrorHandler(422, 'This wallet is already disabled');

        if (currentWallet.status === true) {
            let result = await changeWalletStatus(currentWallet);
            response.status(200).send(result);
        } else {
            throw new ErrorHandler(409, {owned_by: 'This account\'s wallet is already enabled.'});
        }

    } catch (error) {
        next(error);
    }
}

async function changeWalletStatus(wallet) {
    wallet.status = false;
    await wallet.save();

    return {
        status: 'success',
        data: {
            wallet: {
                id: wallet._id,
                owned_by: wallet.owned_by,
                status: (!wallet.status) ? 'disabled' : 'enabled',
                disabled_at: wallet.updated_at,
                balance: wallet.balance
            }
        }
    };
}