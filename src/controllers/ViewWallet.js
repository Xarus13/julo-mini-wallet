require('dotenv').config();
const Account = require('../models/Account');
const Wallet = require('../models/Wallet');
const {ErrorHandler} = require("../helpers/ErrorHandler");

exports.getWalletBalance = async (request, response, next) => {

    try {
        let wallet = await Wallet.findOne({owned_by: response.locals.user._id});

        if (wallet === null || wallet === undefined || wallet.status === false) throw new ErrorHandler(422, 'Wallet is currently disabled or does not exist');

        response.status(200).send({
            status: 'success',
            data: {
                wallet: {
                    id: wallet._id.toHexString(),
                    owned_by: wallet.owned_by,
                    status: (wallet.status) ? 'enabled' : 'disabled',
                    enabled_at: wallet.updated_at,
                    balance: wallet.balance
                }
            }
        });
    } catch (error) {
        next(error);
    }

}