require('dotenv').config();
const Wallet = require('../models/Wallet');
const {ErrorHandler} = require("../helpers/ErrorHandler");

exports.enableWallet = async (request, response, next) => {
    let user = response.locals.user;

    try {
        let currentWallet = await Wallet.findOne({owned_by: user._id});

        if (currentWallet === null) {
            let result = await createWallet(user);
            response.status(201).send(result);
        } else if (currentWallet.status === false) {
            let result = await activateWallet(currentWallet);
            response.status(200).send(result);
        } else {
            throw new ErrorHandler(409, {owned_by: 'This account\'s wallet is already enabled.'});
        }

    } catch (error) {
        next(error);
    }
}

async function activateWallet(wallet) {
    wallet.status = true;
    await wallet.save();

    return {
        status: 'success',
        data: {
            wallet: {
                id: wallet._id,
                owned_by: wallet.owned_by,
                status: (wallet.status) ? 'enabled' : 'disabled',
                enabled_at: wallet.enabled_at,
                balance: wallet.balance
            }
        }
    };
}

async function createWallet(user) {
    let newWallet = await Wallet.create({
        owned_by: user._id,
        status: true,
        balance: 0,
        updated_at: new Date(),
    });

    return {
        status: 'success',
        data: {
            wallet: {
                id: newWallet._id,
                owned_by: newWallet.owned_by,
                status: (newWallet.status) ? 'enabled' : 'disabled',
                enabled_at: newWallet.updated_at,
                balance: newWallet.balance
            }
        }
    };
}