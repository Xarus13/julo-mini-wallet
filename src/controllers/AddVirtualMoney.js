require('dotenv').config();
const Wallet = require('../models/Wallet');
const Transaction = require('../models/Transaction');
const {ErrorHandler} = require("../helpers/ErrorHandler");

exports.addVirtualMoney = async (request, response, next) => {
    const {amount, reference_id} = request.body;
    let user = response.locals.user;

    try {
        let wallet = await Wallet.findOne({owned_by: user._id});

        if (wallet === undefined || wallet === null) throw new ErrorHandler(403, 'This user does not have a wallet');
        if (wallet.status === false) throw new ErrorHandler(400, 'Unable to execute transaction. Wallet is currently disabled');

        let transaction = await Transaction.create({
            type: 'Deposit',
            trigger_by: user._id,
            status: 'success',
            execute_at: new Date,
            amount,
            reference_id
        });

        wallet.balance += amount;
        await wallet.save(() => {
            response.status(201).send({
                status: 'success',
                data: {
                    deposit: {
                        id: transaction._id,
                        deposited_by: user._id,
                        status: 'success',
                        deposited_at: transaction.execute_at,
                        amount,
                        reference_id
                    }
                }
            });
        });

    } catch (error) {
        next(error);
    }
}