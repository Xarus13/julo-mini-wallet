require('dotenv').config();
const crypto = require('crypto');
const {ErrorHandler} = require("../helpers/ErrorHandler");
const Account = require('../models/Account');

exports.createAccount = async (request, response, next) => {
    const customerXID = request.body.customer_xid;

    try {
        if (customerXID === undefined || customerXID === null) throw new ErrorHandler(400, {customer_xid: 'A customer_xid is required'});

        let account = await Account.findOne({customer_xid: customerXID});

        if (account === null) {
            let data = await new Account({
                customer_xid: customerXID,
                token: crypto.randomBytes(20).toString('hex')
            });
            await data.save();

            response.status(201).send({
                status: 'success',
                data: {
                    token: data.token
                }
            });
        } else {
            throw new ErrorHandler(409, {customer_xid: 'An account with this customer_xid already exist.'});
        }
    } catch (error) {
        next(error);
    }
}