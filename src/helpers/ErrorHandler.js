class ErrorHandler extends Error {
    constructor(statusCode, message) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }
}

const handleError = (error, response) => {
    const {statusCode, message} = error;

    // TODO: This is a very simplified representation of HTTP Status Codes.
    //  A reevaluation may be necessary depending on service requirements.
    if (statusCode !== undefined && statusCode !== null) {
        if (statusCode >= 400 && statusCode < 500) {
            response.status(statusCode).send({
                status: 'fail',
                data: message
            });
        } else {
            response.status(statusCode).send({
                status: 'error',
                message
            });
        }
    } else {
        response.status(500).send({
            status: 'error',
            message
        });
    }
};

module.exports = {
    ErrorHandler,
    handleError
}