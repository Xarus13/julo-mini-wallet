const {ErrorHandler} = require("./ErrorHandler");
const Account = require('../models/Account');

exports.authHandler = async (request, response, next) => {
    const {authorization} = request.headers;

    try {
        if (authorization === null || authorization === undefined) throw new ErrorHandler(401, {user: 'User is not authorized'});

        if (authorization.startsWith('Token ')) {

            let token = authorization.split(' ')[1];
            let account = await Account.findOne({token: token}, 'customer_xid token', (error, object) => {
                return object;
            }).clone();

            if (account === null) throw new ErrorHandler(401, {user: 'User is not authorized'});

            response.locals.user = account;
            next();
        } else {
            throw new ErrorHandler(401, {user: 'User is not authorized'});
        }
    } catch (error) {
        next(error);
    }
}