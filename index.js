require('dotenv').config();
const express = require('express');
const {handleError} = require('./src/helpers/ErrorHandler');
const walletRoute = require('./src/routes/wallet');
const initRoute = require('./src/routes/init');
const bodyParser = require('body-parser');
const cors = require('cors');

const mongoose = require('mongoose');
const url = `mongodb://${process.env.DB_HOST + ':' + process.env.DB_PORT + '/julo' || 'localhost:27017/julo'}`;

const app = express();
const port = process.env.PORT || 80;

app.disable('x-powered-by');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

mongoose.connect(
    url,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    (err, client) => {
        if (err) {
            console.error(err);
            return 0;
        }
    }
)

app.use('/api/v1/wallet', walletRoute);
app.use('/api/v1/init', initRoute);

app.use((error, request, response, next) => handleError(error, response))

app.listen(port, () => console.log(`Mini Wallet service is listening to port ${port}.`));

module.exports = app;